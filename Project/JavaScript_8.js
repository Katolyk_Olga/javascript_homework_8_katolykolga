// 1.Опишіть своїми словами що таке Document Object Model (DOM)
// В перекладі означає об'єктна модель документа. Ця система дозволяє отримувати доступ до структури HTMLдокументу,
// із скриптиа, та робити в ньому зміни як на рівні елементів, так і стилізації

 
// 2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// Обидві властивості описують елемент. Однак innerText включає в себе лише внутрішній текст, який розташован поміж тегами елемента.
// А innerHTML включає в себе весь елемент, з урахуванням і тексту, і HTML розмітки (тегів)


// 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Можемо шукати за допомогою різних конструкцій document.getElementById, ...ByTegName, ...ByClass, 
// або document.querySelector як більш універсальний варіант.
// Також маючи якийсь елемент, можемо шукати його батьків, дітей та однорівненвих сусідів за допомогою
// parentNode parentElement childNodes children firstChild, LastElementChild, previousSibling та інщі.


// Код для завдань лежить в папці project.
// - Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// - - Знайти елемент із id="optionsList". Вивести у консоль. 
// - - Знайти батьківський елемент та вивести в консоль. 
// - - Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// - Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// - Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
// Кожному з елементів присвоїти новий клас nav-item.
// - Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.



// - Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// Перший варіант: loop for
// let paragraphs = document.getElementsByTagName("p");
// console.dir(paragraphs);
// for (let i = 0; i < paragraphs.length; i++) {
//     paragraphs[i].style.backgroundColor = "#ff0000";
// };


//  Другий варіант: forEach
let paragraphs = document.querySelectorAll('p');
console.dir(paragraphs);
// Array.from(paragraphs);
// console.dir(paragraphs);
paragraphs.forEach(paragraph => {
    paragraph.style.backgroundColor = "#ff0000"
});

// - - Знайти елемент із id="optionsList". Вивести у консоль. 
let optionsList = document.getElementById("optionsList");
console.dir(optionsList);

// - - Знайти батьківський елемент та вивести в консоль. 
// let parentOptionsList = document.getElementById("optionsList").parentElement;
let parentOptionsList = optionsList.parentElement;
console.dir(parentOptionsList);

// - - Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let listChildNodeOptions = optionsList.childNodes;
// console.dir(document.getElementById("optionsList").childNodes);
console.dir(listChildNodeOptions);
listChildNodeOptions.forEach(itemNode => {
    console.log(`Name: ${itemNode.nodeName}, Type: ${itemNode.nodeType}`)
    // console.log (itemNode.nodeType) //nodeType 1 - element, 2 - attribute, 3 - text, 8 - comment, 9 - document
});

// - Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// !!! В документі немає такого класа, є така id, опрацьовувала цей елемент
let changeTestParagraph = document.querySelector("#testParagraph");
console.dir(changeTestParagraph);
console.log(`Text before changes: ${changeTestParagraph.innerText}`);
changeTestParagraph.innerText = "This is a paragraph";
console.log(`Text after changes: ${changeTestParagraph.innerText}`);

// - Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
// Кожному з елементів присвоїти новий клас nav-item.
let collectionMainHeader = document.querySelector(".main-header");
console.dir(collectionMainHeader);
console.dir(collectionMainHeader.children);
for (let itemMainHeader of collectionMainHeader.children) {
    console.dir(itemMainHeader);
    itemMainHeader.classList.add("nav-item");
    console.dir(itemMainHeader);
};

// - Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
// let collectionSectionTitle = document.getElementsByClassName("section-title");
let collectionSectionTitle = document.querySelectorAll(".section-title");
console.dir(collectionSectionTitle);
// Array.from(collectionSectionTitle);
// console.dir(collectionSectionTitle);
collectionSectionTitle.forEach(function (itemSectionTitle) {
    // console.dir(`Before deleting the class: ${itemSectionTitle}`); // Чомусь цей варіант дивно виводить назву дітей item
    // itemSectionTitle.classList.remove("section-title");
    // console.dir(`After deleting the class: ${itemSectionTitle}`);
    console.dir(itemSectionTitle);
    itemSectionTitle.classList.remove("section-title");
    console.dir(itemSectionTitle);
});
